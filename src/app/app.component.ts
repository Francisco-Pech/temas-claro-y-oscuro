import { Component, HostBinding } from '@angular/core';
import {OverlayContainer} from '@angular/cdk/overlay';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  /* Esto permite el uso de los colores en diferentes componentes, es decir, si se desea en un
  header es ideal su uso. */
  @HostBinding('class') componentCssClass: any;

  constructor(private overlayContainer: OverlayContainer){}

  /**
   * Función que determina el tema a utilizar
   */
  onSetTheme = (data: string) => {
    this.overlayContainer.getContainerElement().classList.add(data);
    this.componentCssClass =  data;
  }

  title = 'change-style-angular-material';
}
