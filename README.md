# Procedimiento
Para este proyecto se hizo uso de Temas para realizar el efecto de temas claros y oscuros. Se logró esto mediante la instalación de
angular material

## Instalación Angular material
Para la instalación se realizo los siguiente pasos:

1. Ubicación de la carpeta del proyect
2. Poner en Angular ClI `ng add @angular/material`
3. Se realizan 3 preguntas de manera consecutiva, referenciadas al tema, 
tipografía y animación(Se recomienda tener por defecto en tema como custom).

Esto genera en el proyecto un archivo con extensión '.scss'. el cual se encuentra en la carpeta `src/`. No se manejan los archivos 
de los componentes, si no se crea uno especial para su uso.

NOTA: En el caso de haber escogido custonm, aparecera uno con nombre 'custom-theme.scss'.

## Verificar unión de Archivo de tema con angular.json
Para comprobar el uso del archivo se va a angular.json y se ve la opción de 'style', ahí debe estar referenciado el archivo.

NOTA:  Se puede crear un archivo independiente y referenciar en angular.json

## Colores definidos
Los colores definidos se pueden crear o bien usar la paleta por defecto de angular material, ubicado en 'node_modules/@angular/material/_themes.scss'.

